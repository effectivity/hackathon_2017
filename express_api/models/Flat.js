module.exports = (
  name,
  description,
  location,
  price,
  area,
  mainImg,
  filters
) => ({
  name,
  description,
  location,
  price,
  area,
  pricePerSqM: (price / area).toFixed(2),
  mainImg,
  filters
})
