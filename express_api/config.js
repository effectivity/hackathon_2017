module.exports = {
  absoluteUrl: 'http://localhost:3333',
  cityArea: {
    minLat: 50.7559721,
    maxLat: 50.7916045,
    minLng: 16.2755263,
    maxLng: 16.3018055
  }
}
