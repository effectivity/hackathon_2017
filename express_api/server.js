const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const path = require('path')

const flatGenerator = require('./utils/seeders/flat')
const db = require('./utils/db')
const filters = require('./utils/filters')
const mapPointGenerator = require('./utils/seeders/mapPoint')

const app = express()

app.use(bodyParser.json())
app.use(cors())
app.use('/assets', express.static(path.join(__dirname, 'assets')))

const valuablePoints = {
  hospitals: mapPointGenerator(10, filters.FILTER_HOSPITALS),
  kindergartens: mapPointGenerator(10, filters.FILTER_KINDERGARTENS),
  schools: mapPointGenerator(10, filters.FILTER_SCHOOLS),
  shops: mapPointGenerator(10, filters.FILTER_SHOPS),
  bus_stops: mapPointGenerator(10, filters.FILTER_BUSSTOPS),
  parks: mapPointGenerator(10, filters.FILTER_PARKS)
}

const flats = flatGenerator(100, valuablePoints)

app.get('/', (req, res) => {
  const filterRawFromGet = req.param('filters') || ''
  const filtersForSearch = Object.values(filters).filter(
    filter => filterRawFromGet.split(',').indexOf(filter) >= 0
  )

  db
    .forBusStops(filtersForSearch)(flats)
    .then(db.forHospitals(filtersForSearch))
    .then(db.forKindergartens(filtersForSearch))
    .then(db.forParks(filtersForSearch))
    .then(db.forSchools(filtersForSearch))
    .then(db.forShops(filtersForSearch))
    .then(flats => {
      let prices = flats.map(flat => flat.price)
      let result = {
        valuablePoints: valuablePoints,
        collection: flats,
        count: flats.length,
        maxPrice: Math.max(...prices),
        minPrice: Math.min(...prices)
      }
      res.status(200).json(result)
    })
})

module.exports = app
