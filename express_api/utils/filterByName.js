const isSameName = filterName => filter => filter.name === filterName

module.exports = filterName => flats =>
  flats.filter(flat => {
    const filters = flat.filters.filter(isSameName(filterName))

    return filters.length > 0 && filters[0].value >= 0.5
  })
