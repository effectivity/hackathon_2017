const FILTER_HOSPITALS = 'hospitals'
const FILTER_KINDERGARTENS = 'kindergartens'
const FILTER_SCHOOLS = 'schools'
const FILTER_SHOPS = 'shops'
const FILTER_BUSSTOPS = 'bus_stops'
const FILTER_PARKS = 'parks'

module.exports = {
  FILTER_HOSPITALS,
  FILTER_KINDERGARTENS,
  FILTER_SCHOOLS,
  FILTER_SHOPS,
  FILTER_BUSSTOPS,
  FILTER_PARKS
}
