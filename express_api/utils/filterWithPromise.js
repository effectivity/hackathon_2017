const filterByName = require('./filterByName')

module.exports = filterName => flats =>
  new Promise((resolve, reject) => resolve(filterByName(filterName)(flats)))
