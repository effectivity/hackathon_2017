const cfg = require('../config')
const Location = require('../models/Location')

module.exports = () => {
  //wybrany obszar miasta
  const minLat = cfg.cityArea.minLat
  const maxLat = cfg.cityArea.maxLat
  const minLng = cfg.cityArea.minLng
  const maxLng = cfg.cityArea.maxLng

  const deltaLat = Math.abs(minLat - maxLat)
  const deltaLng = Math.abs(minLng - maxLng)

  return Location(
    minLat + Math.random() * deltaLat,
    minLng + Math.random() * deltaLng
  )
}
