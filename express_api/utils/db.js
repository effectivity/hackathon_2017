const hospitals = require('./db/hospitals')
const kindergartens = require('./db/kindergartens')
const schools = require('./db/schools')
const shops = require('./db/shops')
const busStops = require('./db/bus_stops')
const parks = require('./db/parks')

module.exports.forHospitals = hospitals
module.exports.forKindergartens = kindergartens
module.exports.forSchools = schools
module.exports.forShops = shops
module.exports.forBusStops = busStops
module.exports.forParks = parks
