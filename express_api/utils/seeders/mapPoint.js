const MapPoint = require('../../models/MapPoint')
const randomLocation = require('../randomLocation')

const names = {
  hospitals: 'Szpital',
  kindergartens: 'Plac zabaw',
  schools: 'Szkoła',
  shops: 'Sklep',
  bus_stops: 'Przystanek autobusowy',
  parks: 'Park'
}

const getNameByType = type => {
  let foundNameKey = Object.keys(names).filter(nameKey => nameKey === type)

  return names[foundNameKey]
}

const createMapPoint = (type, nr) => {
  let name = getNameByType(type) + ' #' + nr
  let location = randomLocation()

  return MapPoint(name, type, location)
}

module.exports = (amount, type) => {
  return [...Array(amount)].map((el, idx) => createMapPoint(type, idx))
}
