const filters = require('../filters')
const cfg = require('../../config')

const Flat = require('../../models/Flat')
const Location = require('../../models/Location')
const Filter = require('../../models/Filter')
const randomLocation = require('../../utils/randomLocation')

const lorem = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
Fusce eget urna non diam aliquam consectetur. Etiam auctor turpis ut libero dictum malesuada. 
Sed ex magna, congue a facilisis tincidunt, feugiat et neque. Aliquam luctus ligula vel lacus pulvinar ornare. 
Pellentesque eget ipsum sit amet quam commodo condimentum eu eget nunc. Sed eget luctus purus, eget luctus lectus. 
Curabitur mattis mollis lacus commodo lobortis. Curabitur volutpat sem sit amet dui blandit, ac vulputate diam consequat. 
Nullam vulputate pharetra tellus, eu feugiat sem. Fusce maximus ipsum in dolor consectetur bibendum. 
Maecenas dictum sem quis erat varius congue.`

const randomPrice = () => (100000 + Math.random() * 200000).toFixed(2)

const randomMainImg = () => {
  let nr = Math.floor(Math.random() * 9)
  return cfg.absoluteUrl + `/assets/img/flats/${nr}.jpg`
}

const randomArea = () => (Math.random() * 100 + 50).toFixed(2)

const calculateFilterValue = (location, valuableLocationArr) => {
  let preIndexArr = valuableLocationArr.map(valuableLocation => {
    let deltaDist =
      Math.pow(location.lat - valuableLocation.location.lat, 2) +
      Math.pow(location.lng - valuableLocation.location.lng, 2)
    return Math.sqrt(deltaDist) * 10 * 7
  })

  let minPreIndex = Math.min(...preIndexArr)
  let index = 1 - minPreIndex
  if (index < 0) index = 0

  return index
}

const createFilters = (location, valuablePoints) =>
  Object.keys(filters).map(filterKey => {
    let filterType = filters[filterKey]

    return Filter(
      filters[filterKey],
      calculateFilterValue(location, valuablePoints[filterType])
    )
  })

const createFlat = (name, valuablePoints) => {
  let location = randomLocation()
  return Flat(
    name,
    lorem,
    location,
    randomPrice(),
    randomArea(),
    randomMainImg(),
    createFilters(location, valuablePoints)
  )
}

module.exports = (amount, valuablePoints) =>
  [...Array(amount)].map((el, idx) => createFlat(`Dom #${idx}`, valuablePoints))
