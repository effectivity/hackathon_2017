const filterWithPromise = require('./filterWithPromise')
const filterByName = require('./filterByName')
const filters = require('./filters')

const hasFilter = (filterName, filtersName) =>
  filtersName.indexOf(filterName) >= 0

module.exports = filterName => filtersName => flats =>
  new Promise((resolve, reject) => {
    if (hasFilter(filterName, filtersName)) {
      resolve(filterByName(filterName)(flats))
    } else {
      resolve(flats)
    }
  })
