const filterWithQuery = require('./../filterWithQuery')
const { FILTER_HOSPITALS } = require('./../filters')

module.exports = filterWithQuery(FILTER_HOSPITALS)
