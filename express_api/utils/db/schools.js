const filterWithQuery = require('./../filterWithQuery')
const { FILTER_SCHOOLS } = require('./../filters')

module.exports = filterWithQuery(FILTER_SCHOOLS)
