const filterWithQuery = require('./../filterWithQuery')
const { FILTER_SHOPS } = require('./../filters')

module.exports = filterWithQuery(FILTER_SHOPS)
