const filterWithQuery = require('./../filterWithQuery')
const { FILTER_PARKS } = require('./../filters')

module.exports = filterWithQuery(FILTER_PARKS)
