const filterWithQuery = require('./../filterWithQuery')
const { FILTER_BUSSTOPS } = require('./../filters')

module.exports = filterWithQuery(FILTER_BUSSTOPS)
