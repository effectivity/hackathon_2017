const filterWithQuery = require('./../filterWithQuery')
const { FILTER_KINDERGARTENS } = require('./../filters')

module.exports = filterWithQuery(FILTER_KINDERGARTENS)
