import styled from 'styled-components'

export const Paragraph = styled.p`
  color: ${props => props.color || 'red'};
`

export const Header = styled.div`
  border-bottom: ${props => props.border_bottom || '1px solid #fff'};
`
