import React from 'react'
import { connect } from 'react-redux'
import GoogleMap from './../../components/GoogleMap'

const mapStateToProps = state => ({
  markers: state.marker.markers
})

export default connect(mapStateToProps)(GoogleMap)
