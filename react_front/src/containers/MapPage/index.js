import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Marker } from 'react-google-maps'
import Maps from './../../components/Maps'
import GoogleMap from './../GoogleMap'
import {
  fetchMarkers,
  changeCurrentPrice
} from './../../store/reducers/markers'

import defaultIcon from './../../assets/markers/marker.png'
import kindergartenIcon from './../../assets/markers/kindergarten.png'
import parksIcon from './../../assets/markers/parks.png'
import schoolsIcon from './../../assets/markers/schools.png'
import busStopsIcon from './../../assets/markers/bus_stops.png'
import hospitalsIcon from './../../assets/markers/hospitals.png'
import shopsIcon from './../../assets/markers/shops.png'

const GoogleMapsURL =
  'https://maps.googleapis.com/maps/api/js?key=AIzaSyCIiCSAkmzSs2acj1UMyC8RL8EF0oslU1A&v=3.exp&libraries=geometry,drawing,places'

class MapPage extends Component {
  constructor(props) {
    super(props)
    this.props.loadMarkers(this.props.filters)
  }

  render() {
    console.log(this.props.dataPointers)

    return (
      <Maps
        markers={this.props.markers}
        currentValue={this.props.currentValue}
        onChange={this.props.onChange}
        minPrice={this.props.minPrice}
        maxPrice={this.props.maxPrice}
      >
        <GoogleMap
          googleMapURL={GoogleMapsURL}
          loadingElement={<div style={{ height: `100%` }} />}
          containerElement={<div style={{ height: `800px` }} />}
          mapElement={<div style={{ height: `100%` }} />}
          defaultCenter={this.props.center}
        >
          {this.props.markers &&
            this.props.markers.map((marker, index) => (
              <Marker key={index} options={{ icon: { url: defaultIcon, scaledSize: {width: 30, height: 48} }}} position={marker.location} />
            ))}

          {this.props.showDataPointers &&
            this.props.dataPointers['shops'].map((marker, index) => (
              <Marker key={index} options={{ icon: { url: shopsIcon, scaledSize: {width: 30, height: 48} }}} position={marker.location} />
            ))}
          {this.props.showDataPointers &&
            this.props.dataPointers['parks'].map((marker, index) => (
              <Marker key={index} options={{ icon: { url: parksIcon, scaledSize: {width: 30, height: 48} }}} position={marker.location} />
            ))}
          {this.props.showDataPointers &&
            this.props.dataPointers['bus_stops'].map((marker, index) => (
              <Marker key={index} options={{ icon: { url: busStopsIcon, scaledSize: {width: 30, height: 48} }}} position={marker.location} />
            ))}
          {this.props.showDataPointers &&
            this.props.dataPointers['schools'].map((marker, index) => (
              <Marker key={index} options={{ icon: { url: schoolsIcon, scaledSize: {width: 30, height: 48} }}} position={marker.location} />
            ))}
          {this.props.showDataPointers &&
            this.props.dataPointers['kindergartens'].map((marker, index) => (
              <Marker key={index} options={{ icon: { url: kindergartenIcon, scaledSize: {width: 30, height: 48} }}} position={marker.location} />
            ))}
          {this.props.showDataPointers &&
            this.props.dataPointers['hospitals'].map((marker, index) => (
              <Marker key={index} options={{ icon: { url: hospitalsIcon, scaledSize: {width: 30, height: 48} }}} position={marker.location} />
            ))}
        </GoogleMap>
      </Maps>
    )
  }
}

const mapStateToProps = state => ({
  markers: state.marker.markers,
  filters: state.filter.filters,
  center: {
    lat: state.marker.centerLat,
    lng: state.marker.centerLng
  },
  minPrice: state.marker.minPrice,
  maxPrice: state.marker.maxPrice,
  currentValue: state.marker.currentValue,
  dataPointers: state.marker.dataPointers,
  showDataPointers: state.marker.showDataPointers
})

const bindDispatchToProps = dispatch => ({
  loadMarkers: bindActionCreators(fetchMarkers, dispatch),
  onChange: bindActionCreators(changeCurrentPrice, dispatch)
})

export default connect(mapStateToProps, bindDispatchToProps)(MapPage)
