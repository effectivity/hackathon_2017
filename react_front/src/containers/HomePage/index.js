import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import EntryContainer from './../../components/Container'
import CategoryItem from './../../components/CategoryItem'
import SuperHalfWrap from './../../components/SuperHalfWrap'
import { toggleFilter, togglePacket } from '../../store/reducers/filters'

const filterByColumn = columnNr => filters => key =>
  filters[key].column === columnNr

const HomePage = ({ filters, toggleFilter, togglePacket }) => (
  <EntryContainer>
    <SuperHalfWrap title="Kto szuka?">
      {Object.keys(filters)
        .filter(filterByColumn(0)(filters))
        .map(filter => (
          <CategoryItem
            {...filters[filter]}
            filterName={filter}
            selectFilter={togglePacket}
          />
        ))}
    </SuperHalfWrap>
    <SuperHalfWrap title="Czego szukasz?">
      {Object.keys(filters)
        .filter(filterByColumn(1)(filters))
        .map(filter => (
          <CategoryItem
            {...filters[filter]}
            filterName={filter}
            selectFilter={toggleFilter}
          />
        ))}
    </SuperHalfWrap>
  </EntryContainer>
)

const mapStateToProps = state => ({
  filters: state.filter.filters
})
const bindDispatchToProps = dispatch => ({
  toggleFilter: bindActionCreators(toggleFilter, dispatch),
  togglePacket: bindActionCreators(togglePacket, dispatch)
})

export default connect(mapStateToProps, bindDispatchToProps)(HomePage)
