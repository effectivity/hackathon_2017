import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import FooterAction from './../../components/FooterAction'
import { showDataPointers } from './../../store/reducers/markers'

const mapStateToProps = state => ({
  label1: 'Powrót do filtrów',
  label2: 'Wizualizacja filtrów',
  url: '/'
})
const bindDispatchToProps = dispatch => ({
  action: bindActionCreators(showDataPointers, dispatch)
})

export default connect(mapStateToProps, bindDispatchToProps)(FooterAction)
