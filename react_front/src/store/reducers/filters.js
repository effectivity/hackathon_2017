import filters from './../../utils/filters'

export const TOGGLE_FILTER = 'filters/TOGGLE_FILTER'
export const TOGGLE_FAMILY = 'filters/TOGGLE_FAMILY'
export const TOGGLE_SINGLE = 'filters/TOGGLE_SINGLE'

const initialState = {
  filters
}

export default (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_FILTER:
      const isFamily =
        (state.filters['hospitals'].isSelected ||
          action.name === 'hospitals') &&
        (state.filters['kindergartens'].isSelected ||
          action.name === 'kindergartens') &&
        (state.filters['schools'].isSelected || action.name === 'schools') &&
        (['hospitals', 'kindergartens', 'schools'].indexOf(action.name) < 0 ||
          !state.filters[action.name].isSelected)

      const isSingle =
        (state.filters['shops'].isSelected || action.name === 'shops') &&
        (state.filters['bus_stops'].isSelected ||
          action.name === 'bus_stops') &&
        (state.filters['parks'].isSelected || action.name === 'parks') &&
        (['shops', 'bus_stops', 'parks'].indexOf(action.name) < 0 ||
          !state.filters[action.name].isSelected)

      return {
        ...state,
        filters: {
          ...state.filters,
          single: {
            ...state.filters['single'],
            isSelected: isSingle
          },
          family: {
            ...state.filters['family'],
            isSelected: isFamily
          },
          [action.name]: {
            ...state.filters[action.name],
            isSelected: !state.filters[action.name].isSelected
          }
        }
      }
    case TOGGLE_SINGLE:
      return {
        ...state,
        filters: {
          ...state.filters,
          single: {
            ...state.filters['single'],
            isSelected: true
          },
          shops: {
            ...state.filters['shops'],
            isSelected: true
          },
          bus_stops: {
            ...state.filters['bus_stops'],
            isSelected: true
          },
          parks: {
            ...state.filters['parks'],
            isSelected: true
          }
        }
      }
    case TOGGLE_FAMILY:
      return {
        ...state,
        filters: {
          ...state.filters,
          family: {
            ...state.filters['family'],
            isSelected: true
          },
          hospitals: {
            ...state.filters['hospitals'],
            isSelected: true
          },
          kindergartens: {
            ...state.filters['kindergartens'],
            isSelected: true
          },
          schools: {
            ...state.filters['schools'],
            isSelected: true
          }
        }
      }
    default:
      return state
  }
}

export const toggleFilter = name => dispatch =>
  dispatch({ type: TOGGLE_FILTER, name })

export const togglePacket = packet => dispatch => {
  console.log(packet)
  switch (packet) {
    case 'single':
      return dispatch({ type: TOGGLE_SINGLE })
    case 'family':
      return dispatch({ type: TOGGLE_FAMILY })
  }
}
