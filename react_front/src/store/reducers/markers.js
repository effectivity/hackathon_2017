import 'whatwg-fetch'

const MARKERS_LOADED = 'markers/MARKERS_LOADED'
const CENTER_MAP = 'markers/CENTER_MAP'
const CONFIG_PRICES = 'markers/CONFIG_PRICES'
const CONFIG_CURRENT_PRICE = 'markers/CONFIG_CURRENT_PRICE'
const CONFIG_DATA_POINTERS = 'markers/CONFIG_DATA_POINTERS'
const CONFIG_SHOW_DATA_POINTERS = 'markers/CONFIG_SHOW_DATA_POINTERS'

const initialState = {
  markers: [],
  rawMarkers: [],
  minPrice: 0,
  maxPrice: 0,
  currentValue: 0,
  centerLat: 50.784,
  centerLng: 16.2844,
  dataPointers: {
    hospitals: [],
    shops: [],
    kindergartens: [],
    schools: [],
    bus_stops: [],
    parks: []
  },
  showDataPointers: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case MARKERS_LOADED:
      return { ...state, markers: action.markers, rawMarkers: action.markers }
    case CONFIG_PRICES:
      return {
        ...state,
        minPrice: action.minPrice,
        maxPrice: action.maxPrice,
        currentValue: action.maxPrice
      }
    case CONFIG_CURRENT_PRICE:
      return {
        ...state,
        currentValue: action.value,
        markers: state.rawMarkers.filter(marker => marker.price <= action.value)
      }
    case CONFIG_DATA_POINTERS:
      return {
        ...state,
        dataPointers: action.pointers
      }
    case CONFIG_SHOW_DATA_POINTERS:
      return {
        ...state,
        showDataPointers: !state.showDataPointers
      }
    case CENTER_MAP:
      return {
        ...state,
        centerLat: action.centerLat,
        centerLng: action.centerLng
      }
    default:
      return state
  }
}

export const fetchMarkers = filters => dispatch => {
  const rawFilters = Object.keys(filters)
    .filter(filter => filters[filter].isSelected)
    .join(',')

  fetch(`http://localhost:3333?filters=${rawFilters}`)
    .then(res => res.json())
    .then(results => {
      dispatch({ type: MARKERS_LOADED, markers: results.collection })
      return results
    })
    .then(results => {
      dispatch({
        type: CONFIG_PRICES,
        minPrice: Math.round(results.minPrice / 100000) * 100000,
        maxPrice: Math.ceil(results.maxPrice / 100000) * 100000
      })
      return results
    })
    .then(results => {
      dispatch({
        type: CONFIG_DATA_POINTERS,
        pointers: results.valuablePoints
      })
      return results
    })
    .then(results => results.collection)
    .then(results => {
      const LAT = results
        .map(result => result.location.lat)
        .reduce((prev, next) => prev + next)

      const LNG = results
        .map(result => result.location.lng)
        .reduce((prev, next) => prev + next)

      dispatch({
        type: CENTER_MAP,
        centerLat: LAT / results.length,
        centerLng: LNG / results.length
      })
    })
}

export const changeCurrentPrice = value => dispatch => {
  dispatch({ type: CONFIG_CURRENT_PRICE, value })
}

export const showDataPointers = () => dispatch => {
  dispatch({ type: CONFIG_SHOW_DATA_POINTERS })
}
