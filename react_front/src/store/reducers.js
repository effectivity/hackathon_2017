import { combineReducers } from 'redux'
import filterReducers from './reducers/filters'
import markerReducers from './reducers/markers'

export default combineReducers({
  filter: filterReducers,
  marker: markerReducers
})
