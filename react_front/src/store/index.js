import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import reducers from './reducers'

const initialStore = {}
const middlewares = [thunk]
const enhancers = []

const composedEnhacers = compose(applyMiddleware(...middlewares), ...enhancers)

export default createStore(reducers, initialStore, composedEnhacers)
