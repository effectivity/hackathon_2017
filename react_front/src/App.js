import React, { Component } from 'react'
import { BrowserRouter, Route } from 'react-router-dom'
import Header from './components/Header'
import Footer from './components/Footer'
import FooterDev from './containers/FooterAction'
import HomePage from './containers/HomePage'
import MapPage from './containers/MapPage'
import withFooter from './hoc/withFooter'

const footerHomePage = {
  url: '/szukaj',
  label: 'Przejdź dalej'
}

const HomePageWithFooter = withFooter(HomePage)(Footer)(footerHomePage)
const MapPageDevWithFooter = withFooter(MapPage)(FooterDev)({})

const App = () => (
  <BrowserRouter>
    <div>
      <Header siteName="Come2Home" />
      <Route exact path="/" component={HomePageWithFooter} />
      <Route exact path="/szukaj" component={MapPageDevWithFooter} />
    </div>
  </BrowserRouter>
)

export default App
