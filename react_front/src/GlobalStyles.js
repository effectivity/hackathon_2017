import { injectGlobal } from 'styled-components'
import bgImage from './assets/images/site-bg.jpg'

export const globalVariables = {
  primaryWhite: '#f3f3f3',
  primaryGreen: '#4DBF56'
}

injectGlobal`
    body {
        background: url(${bgImage}) center no-repeat;
        background-size: cover;
        font-family: 'Poppins', sans-serif;
        min-height: 100vh;
        margin: 0
    }

    a {
      text-decoration: none;
      &:hover {
        cursor: pointer;
        color: ${globalVariables.primaryGreen}
      }
    }

    * {
      box-sizing: border-box;
    }
`
