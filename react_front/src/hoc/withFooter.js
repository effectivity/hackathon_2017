import React from 'react'

export default Component => Footer => props => () => (
  <div>
    <Component />
    <Footer {...props} />
  </div>
)
