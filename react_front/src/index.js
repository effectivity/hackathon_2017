import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import App from './App'
import './GlobalStyles'
import registerServiceWorker from './registerServiceWorker'
import reduxStore from './store'

ReactDOM.render(
  <Provider store={reduxStore}>
    <App />
  </Provider>,
  document.getElementById('root')
)
registerServiceWorker()
