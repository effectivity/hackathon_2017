import React from 'react'
import Slider from 'react-rangeslider'
import 'react-rangeslider/lib/index.css'
import { ResultContent } from './styles'
import ResultItem from './../ResultItem'

export default ({ markers, minPrice, maxPrice, currentValue, onChange }) => (
  <ResultContent>
    <Slider
      min={minPrice}
      max={maxPrice}
      step={50000}
      value={currentValue}
      onChange={onChange}
      format={value => `${value} PLN`}
    />
    {markers.map((marker, index) => <ResultItem key={index} {...marker} />)}
  </ResultContent>
)
