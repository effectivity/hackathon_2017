import styled from 'styled-components'

export const ResultContent = styled.div`
  width: 100%;
  height: 800px;
  padding: 20px 10px 0 10px;
  background: rgba(255, 255, 255, 0.2);
  display: flex;
  flex-flow: row wrap;
  overflow: auto;

  > .rangeslider {
    position: relative;
    width: calc(100% - 60px);
    margin: 40px 0 40px 30px;

    @media (max-width: 640px) {
      width: 100%;
      margin: 20px 0;
    }

    .rangeslider__handle-tooltip {
      width: 120px;
    }

    &:before {
      content: 'Cena minimalna';
      position: absolute;
      top: -30px;
      left: 0;
      font-size: 12px;
      color: #fff;
      z-index: -1;

      @media (max-width: 640px) {
        display: none;
      }
    }

    &:after {
      content: 'Cena maksymalna';
      position: absolute;
      top: -30px;
      right: 0;
      font-size: 12px;
      color: #fff;
      z-index: -1;

      @media (max-width: 640px) {
        display: none;
      }
    }
  }
`
