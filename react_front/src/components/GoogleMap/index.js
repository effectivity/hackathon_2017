import React from 'react'
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker
} from 'react-google-maps'

const MapWithMarker = ({
  defaultZoom = 16,
  defaultCenter = { lat: 50.784, lng: 16.284 },
  children
}) => (
  <GoogleMap defaultZoom={16} defaultCenter={defaultCenter}>
    {children}
  </GoogleMap>
)

export default withScriptjs(withGoogleMap(MapWithMarker))
