import styled from 'styled-components'

export const Figure = styled.figure`
  position: relative;
  text-align: center;

  &:before {
    content: '';
    position: absolute;
    top: 0;
    left: 50%;
    width: 1px;
    height: 100%;
    background: #222;
    transform: rotate(-30deg);
    display: ${props => (props.value >= 0.5 ? 'none' : 'block')};
  }
`

export const SpanBackground = styled.span`
  position: relative;
  opacity: ${props => (props.value >= 0.5 ? 1 : 0.3)};
`
