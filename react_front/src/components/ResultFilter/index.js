import React from 'react'
import { Figure, SpanBackground } from './styles'

const ResultFilter = ({ logo, color, value }) => (
  <Figure value={value}>
    <SpanBackground className={logo} backgroundColor={color} value={value} />
  </Figure>
)

export default ResultFilter
