import styled from 'styled-components'

// Grid Container
export const GridContainer = styled.div`
  display: flex;
  width: 100%;
  height: ${props => props.height || 'auto'};
  margin: 0 auto;
  max-width: 1600px;
`
