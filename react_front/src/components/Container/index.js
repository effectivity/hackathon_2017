import React from 'react'
import { GridContainer } from './styles'
import CategoryItem from './../CategoryItem'

const Container = ({ children }) => <GridContainer>{children}</GridContainer>

export default Container
