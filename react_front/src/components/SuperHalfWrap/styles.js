import styled from 'styled-components'

// Half Wrapper
export const HalfWrap = styled.div`
  float: left;
  width: 50%;
  height: 100%;
  padding: 30px 0;
  display: flex;
  flex-flow: row wrap;
  justify-content: center;
  align-items: stretch;

  &:nth-child(2) {
    border-right: 1px dashed rgba(255, 255, 255, 0.3);
    border-left: 1px dashed rgba(255, 255, 255, 0.3);
  }

  &:last-child {
    border-right: none;
  }
`

// Category
export const CategoryContent = styled.div`
  float: left;
  width: 100%;
  padding: 0 20px;
  min-height: inherit;
  display: flex;
  flex-flow: row wrap;
  align-items: center;
  justify-content: center;

  @media (max-width: 640px) {
    flex-flow: column;
    flex-direction: column;

    * > {
      width: 100%;
    }
  }
`

// Title
export const Title = styled.div`
  float: left;
  width: 100%;
  margin-bottom: 30px;
  font-size: 26px;
  line-height: 32px;
  font-weight: 600;
  text-align: center;
  color: #fff;
`
