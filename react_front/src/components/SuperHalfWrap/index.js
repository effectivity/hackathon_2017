import React from 'react'
import { Title, HalfWrap, CategoryContent } from './styles'

export const SuperHalfWrap = ({ children, title }) => (
  <HalfWrap>
    <CategoryContent>
      <Title>{title}</Title>
      {children}
    </CategoryContent>
  </HalfWrap>
)

export default SuperHalfWrap
