import styled from 'styled-components'
import { NavLink } from 'react-router-dom'

// Footer
export const Footer = styled.div`
  width: 100%;
  height: 100px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-top: 1px dashed rgba(255, 255, 255, 0.3);
`

export const Href = styled.a`
  display: inline-block;
  width: 200px;
  height: 50px;
  border-radius: 4px;
  text-align: center;
  font-size: 16px;
  line-height: 48px;
  color: #fff;
  border: 1px solid rgba(255, 255, 255, 0.6);
  background: rgba(255, 255, 255, 0.1);
  transition: all 0.3s ease;

  &:hover {
    border: 1px solid rgba(255, 255, 255, 1);
    background: rgba(255, 255, 255, 0.3);
    color: #fff;
  }

  & + a {
    margin-left: 10px;
  }
`

export const NavHref = Href.withComponent(NavLink)
