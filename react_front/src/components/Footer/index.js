import React from 'react'
import { Footer, NavHref } from './styles'

export default ({ url, label }) => (
  <Footer>
    <NavHref to={url}>{label}</NavHref>
  </Footer>
)
