import styled from 'styled-components'

// Category
export const Item = styled.a`
  float: left;
  width: calc(33.333333% - 20px);
  height: auto;
  padding: 10px;
  margin: 0 10px 20px;
  border: 1px dashed #c2c2c2;
  border-radius: 6px;
  transition: all 0.3s ease;
  text-align: center;
  cursor: pointer;

  @media (max-width: 640px) {
    width: 100%;
  }

  &.selected {
    background: rgba(255, 255, 255, 0.3);
    border: 1px solid rgba(255, 255, 255, 0.6);
  }

  &:hover {
    background: rgba(255, 255, 255, 0.3);
    border: 1px dashed transparent;
  }

  figure {
    position: relative;
    display: inline-block;
    padding: 5px;
    margin: 0;
    width: 50px;
    height: 50px;
    border-radius: 100%;
    text-align: center;
    overflow: hidden;
    box-sizing: border-box;
    background: ${props => props.color || '#7c1e4e'};

    @media (max-width: 640px) {
      display: none;
    }

    > span {
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      font-size: 25px;
      color: #f4f4f4;
    }
  }

  > p {
    margin: 0;
    padding: 0;
    font-size: 14px;
    color: #fff;
    text-align: center;
    text-transform: uppercase;
  }
`
