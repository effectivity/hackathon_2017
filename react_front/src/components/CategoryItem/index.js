import React from 'react'
import { Item } from './styles'

const CategoryItem = ({
  logo,
  name,
  color = 'red',
  selectFilter,
  filterName,
  isSelected = false
}) => (
  <Item
    color={color}
    className={isSelected && 'selected'}
    onClick={() => selectFilter(filterName)}
  >
    <figure>
      <span className={logo} />
    </figure>
    <p>{name}</p>
  </Item>
)

export default CategoryItem
