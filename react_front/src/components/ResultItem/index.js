import React from 'react'
import {
  ResultItem,
  Images,
  Price,
  Description,
  Size,
  PriceM2,
  Filters
} from './styles'
import ResultFilter from './../ResultFilter'
import globalFilters from './../../utils/filters'

const ResultPlace = ({
  name,
  description,
  mainImg,
  price,
  pricePerSqM,
  area,
  filters
}) => (
  <ResultItem>
    <Images>
      <img src={mainImg} alt={name} />
      <Price>{price} PLN</Price>
      <PriceM2>{pricePerSqM} PLN / m2</PriceM2>
      <Size>
        {area}
        <br />m2
      </Size>
    </Images>
    <Description>
      <h2>{name}</h2>
      <p>{description.substring(0, 100)}...</p>
    </Description>
    <Filters>
      {filters.map((filter, index) => (
        <ResultFilter
          key={index}
          {...globalFilters[filter.name]}
          value={filter.value}
        />
      ))}
    </Filters>
  </ResultItem>
)

export default ResultPlace
