import styled from 'styled-components'
import globalFilters from './../../utils/filters'

export const ResultItem = styled.div`
  width: calc(50% - 20px);
  margin: 0 10px 20px;
  background: #f2f2f2;
  border-radius: 4px;
  overflow: hidden;

  @media (max-width: 640px) {
    width: 100%;
  }
`
export const Images = styled.div`
  position: relative;
  height: 200px;
  overflow: hidden;

  > img {
    display: block;
    width: 100%;
    height: 200px;
    object-fit: cover;
  }
`
export const Price = styled.span`
  position: absolute;
  top: 10px;
  left: 10px;
  width: auto;
  padding: 3px 7px;
  font-size: 12px;
  color: #fff;
  background: #ee7074;
`
export const PriceM2 = styled.span`
  position: absolute;
  top: 40px;
  left: 10px;
  width: auto;
  padding: 3px 7px;
  font-size: 12px;
  color: #fff;
  background: #ee7074;
`
export const Size = styled.span`
  position: absolute;
  top: 10px;
  right: 10px;
  width: 50px;
  height: 50px;
  padding-top: 13px;
  line-height: 14px;
  font-size: 10px;
  border-radius: 100%;
  color: #fff;
  text-align: center;
  background: #03bb3d;
`
export const Description = styled.div`
  width: 100%;
  padding: 10px 15px;

  > h2 {
    margin: 0 0 10px 0;
    padding: 0;
    font-size: 20px;
    line-height: 24px;
    color: #222;
  }

  > p {
    margin: 0;
    padding: 0;
    font-size: 14px;
    line-height: 18px;
    color: #636363;
  }
`

export const Filters = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-wrap: nowrap;

  & > * {
    width: calc(
      100% /
        ${Object.keys(globalFilters).filter(
          filter => globalFilters[filter].column > 0
        ).length}
    );
    margin: 10px 0;
    text-align: center;
  }
`
