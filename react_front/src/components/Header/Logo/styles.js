import styled from 'styled-components'

export const Logo = styled.div`
  display: flex;
  align-items: center;
  padding: 30px 30px 30px 0;

  @media (max-width: 640px) {
    padding: 10px 10px 10px 0;
  }
`

export const LogoImage = styled.div`
  display: inline-block;
  background: url(${props => props.src}) center no-repeat;
  background-size: contain;
  width: 25px;
  height: 30px;
  margin-right: 10px;
`

export const LogoText = styled.p`
  font-size: 27px;
  font-weight: bold;
  display: inline-block;
  color: #f3f3f3;
  line-height: 1;
  margin: 0;
`
