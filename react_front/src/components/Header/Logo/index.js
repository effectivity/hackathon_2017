import React, { Component } from 'react'
import { Logo, LogoImage, LogoText } from './styles'
import logoImage from '../../../assets/images/logo.png'

export default ({ siteName }) => (
  <Logo>
    <LogoImage src={logoImage} />
    <LogoText>{siteName}</LogoText>
  </Logo>
)
