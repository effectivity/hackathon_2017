import React, { Component } from 'react'
import { Header } from './styles'
import { GridContainer } from '../Container/styles'
import { NavLink } from 'react-router-dom'
import Logo from './Logo'

export default ({ siteName }) => (
  <Header>
    <GridContainer display="flex">
      <NavLink to="/">
        <Logo siteName={siteName} />
      </NavLink>
    </GridContainer>
  </Header>
)
