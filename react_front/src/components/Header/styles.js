import styled from 'styled-components'

export const Header = styled.div`
  border-bottom: ${props =>
    props.border_bottom || '1px solid rgba(255,255,255,.2)'};
  padding: 0 80px;
  overflow: hidden;
  display: flex;
  align-items: center;
  background: rgba(255, 255, 255, 0.1);
`
