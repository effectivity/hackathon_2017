import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import SearchForm from './SearchForm'

export default class ContactForm extends Component {
  constructor() {
    super()
    this.state = {
      fireRedirect: false
    }
  }

  setSearchPhrase = e => {
    this.setState({ searchPhrase: e.target.value, fireRedirect: false })
  }

  submitForm = e => {
    e.preventDefault()
    if (this.state.searchPhrase) {
      this.setState({ fireRedirect: true })
    }
  }

  render() {
    const { fireRedirect } = this.state
    return (
      <div>
        <SearchForm
          submitForm={this.submitForm}
          onChangeSearch={this.setSearchPhrase}
        />
        {fireRedirect && <Redirect to={'/search/' + this.state.searchPhrase} />}
      </div>
    )
  }
}
