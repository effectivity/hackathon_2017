import React, { Component } from 'react'
import { SearchPlace } from './styles'

export default props => (
  <SearchPlace>
    <form onSubmit={props.submitForm}>
      <label>
        <i className="fa fa-search" />
        <input
          id="searchPlace"
          placeholder="Szukaj miejsca"
          onChange={props.onChangeSearch}
        />
      </label>
    </form>
  </SearchPlace>
)
