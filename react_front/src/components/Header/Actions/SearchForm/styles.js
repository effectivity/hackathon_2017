import styled from 'styled-components'
import { globalVariables } from '../../../../GlobalStyles'

export const SearchPlace = styled.div`
  @media (min-width: 768px) {
    input {
      font-size: 14px;
      line-height: 34px;
      padding: 0 20px;
      height: auto;
      width: auto;
    }
  }

  & label {
    display: block;
    text-align: center;
    padding: 5px 5px;
    border-bottom: 1px solid ${globalVariables.primaryGreen};
    color: ${globalVariables.primaryWhite};
  }

  & input {
    background: transparent;
    border: none;
    color: ${globalVariables.primaryWhite};
  }
  & input:focus {
    outline: none;
  }

  > a:hover {
    color: ${globalVariables.primaryGreen};
    cursor: pointer;
  }
`
