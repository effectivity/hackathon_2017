import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import { Actions } from './styles'
import SearchForm from './SearchForm'

export default () => (
  <Actions>
    <SearchForm />
  </Actions>
)
