import styled from 'styled-components'
import { globalVariables } from '../../../GlobalStyles'

export const Actions = styled.div`
  margin-left: auto;
  display: flex;
  align-items: center;
`

export const Account = styled.a`
  display: block;
  width: 23px;
  height: 23px;
  margin-left: 20px;

  &:hover g {
    fill: ${globalVariables.primaryGreen};
  }
`

export const AccountSvg = styled.svg`
  width: 100%;
  height: 100%;
`
