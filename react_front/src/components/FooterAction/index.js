import React from 'react'
import { Footer, Href, NavHref } from './../Footer/styles'

export default ({ action, url, label1, label2 }) => (
  <Footer>
    {url && <NavHref to={url}>{label1}</NavHref>}
    {action && <Href onClick={action}>{label2}</Href>}
  </Footer>
)
