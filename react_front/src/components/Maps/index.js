import React, { Component } from 'react'
import { MapsContainer, Wrapper } from './styles'
import { GridContainer } from '../Container/styles'
import ResultContent from './../Result'

export default ({
  children,
  markers = [],
  minPrice,
  maxPrice,
  onChange,
  currentValue
}) => (
  <div>
    <Wrapper>
      <ResultContent
        markers={markers}
        minPrice={minPrice}
        maxPrice={maxPrice}
        onChange={onChange}
        currentValue={currentValue}
      />
    </Wrapper>

    <Wrapper>
      <MapsContainer>{children}</MapsContainer>
    </Wrapper>
  </div>
)
