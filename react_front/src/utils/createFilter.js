export default ({ logo, name, color, column, isSelected = false }) => ({
  logo,
  name,
  color,
  column,
  isSelected
})
