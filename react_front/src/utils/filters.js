import createFilter from './createFilter'

export default {
  // First column
  single: createFilter({
    logo: 'fa fa-male',
    name: 'Single',
    color: '#4aadf8',
    column: 0
  }),
  family: createFilter({
    logo: 'fa fa-users',
    name: 'Rodzina',
    color: '#9c29f6',
    column: 0
  }),
  // Second column
  hospitals: createFilter({
    logo: 'fa fa-hospital-o',
    name: 'Szpitale',
    color: '#4aadf8',
    column: 1
  }),
  kindergartens: createFilter({
    logo: 'fa fa-grav',
    name: 'Przedszkola',
    color: '#9c29f6',
    column: 1
  }),
  schools: createFilter({
    logo: 'fa fa-graduation-cap',
    name: 'Szkoły',
    color: '#4aadf8',
    column: 1
  }),
  shops: createFilter({
    logo: 'fa fa-shopping-basket',
    name: 'Sklepy',
    color: '#9c29f6',
    column: 1
  }),
  bus_stops: createFilter({
    logo: 'fa fa-bus',
    name: 'Przystanki autobusowe',
    color: '#4aadf8',
    column: 1
  }),
  parks: createFilter({
    logo: 'fa fa-pagelines',
    name: 'Parki',
    color: '#9c29f6',
    column: 1
  })
}
